﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MovingFinger : MonoBehaviour {

	private const int MIN_FIGURE_POINTS = 10;

	[SerializeField]
	private GameController _gameController;
	[SerializeField]
	private TrailRenderer _trailRenderer;
	[SerializeField]
	private ParticleSystem _cometTrail;
	
	private Transform _cachedTransform;
	private Camera _cachedCamera;
	private Vector2 _lastMousePos = Vector2.zero;

	private List<Vector3> _figurePoints = new List<Vector3>();

	void Awake()
	{
		_cachedTransform = transform;
		_cachedCamera = Camera.main;
	}
	
	void Update () {

		if(Input.GetMouseButtonDown(0))
		{
			_lastMousePos = Input.mousePosition;
			ClearFigurePoints();
			StartCoroutine(ClearTrail());

			_cometTrail.Play();
		}

		if(Input.GetMouseButton(0))
		{
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10;

			Vector3 worldPos = _cachedCamera.ScreenToWorldPoint(mousePos);
			_cachedTransform.position = worldPos;

			if(_figurePoints.Count > 0)
			{
				if(Vector3.Distance(_figurePoints[_figurePoints.Count - 1], worldPos) > 0.01f)
				{
					_figurePoints.Add(worldPos);
				}
			}
			else {
				_figurePoints.Add(worldPos);
			}

			UpdateCometTrail(Input.mousePosition);
			_lastMousePos = Input.mousePosition;
		}

		if(Input.GetMouseButtonUp(0))
		{
			_cometTrail.Stop();
			if(_figurePoints.Count > MIN_FIGURE_POINTS)
				_gameController.CheckDrawingCapability(_figurePoints);
		}
	}

	void OnDisable()
	{
		_cometTrail.Stop();
	}

	private void UpdateCometTrail(Vector2 mouseP)
	{

	}

	private IEnumerator ClearTrail()
	{
		float currentTimeTrail = _trailRenderer.time;
		_trailRenderer.time = 0f;
		yield return 0;
		_trailRenderer.time = currentTimeTrail;
	}

	private void ClearFigurePoints()
	{
		_figurePoints.Clear();
	}
}
