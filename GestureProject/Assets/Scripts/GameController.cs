﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	private const float ROUND_TIME = 10f;
	private const float MIN_ROUND_TIME = 5f;

	[SerializeField]
	private GameObject _mainMenuCanvas;
	[SerializeField]
	private GameObject _gameCanvas;
	[SerializeField]
	private GameObject _gameOverCanvas;
	[SerializeField]
	private Image _imageTask;
	[SerializeField]
	private Text _processLabel;
	[SerializeField]
	private Text _scoreLabel;
	[SerializeField]
	private Text _timeLabel;
	[SerializeField]
	private Text _totalScoreLabel;

	private Coroutine RemoveGameMessage_coroutine;
	private bool RemoveGameMessage_running = false;
	private int _currentFigureType = -1;
	private List<Figure> _figures = new List<Figure>();
	private int _score = 0;
	private float _roundTime = ROUND_TIME;

	void Awake()
	{
		InitFigures();
	}

	public void RestartGame()
	{
		_score = 0;
		_currentFigureType = -1;
		_roundTime = ROUND_TIME;

		ShowGameScreen();
		ShowNextFigure();

		StartCoroutine(CalculatingRoundTime());
	}

	public void StartGame()
	{
		ShowGameScreen();
		ShowNextFigure();

		StartCoroutine(CalculatingRoundTime());
	}

	private void ShowGameScreen()
	{
		_mainMenuCanvas.SetActive(false);
		_gameOverCanvas.SetActive(false);
		_gameCanvas.SetActive(true);
	}

	private void InitFigures()
	{
		_figures.Add(new Figure(Figure.FigureType.Square, 0.87f));
		_figures.Add(new Figure(Figure.FigureType.Triangle, 0.8f));
		_figures.Add(new Figure(Figure.FigureType.Circle, 0.65f));
	}

	private void ShowNextFigure()
	{
		int newType = 0;
		do
		{
			newType = Random.Range(0, _figures.Count);
		} while (_currentFigureType == newType);
		_currentFigureType = newType;

		_figures[_currentFigureType].ShowFigure(_imageTask);
	}

	public void CheckDrawingCapability(List<Vector3> drawedPoints)
	{
		if(RemoveGameMessage_running)	{
			StopCoroutine(RemoveGameMessage_coroutine);

			RemoveGameMessage_running = false;
		}

		if(_figures[_currentFigureType].IsFigureCapable(drawedPoints))
		{
			_processLabel.text = "Right!";
			RemoveGameMessage_coroutine = StartCoroutine(RemoveGameMessage());

			NextRound();
		}
		else
		{
			_processLabel.text = "Wrong!";
			RemoveGameMessage_coroutine = StartCoroutine(RemoveGameMessage());
		}
	}

	private void NextRound()
	{
		ScoreFigure();
		ShowNextFigure();
	}

	private void ScoreFigure()
	{
		UpdateRoundTime();

		_score++;
		_scoreLabel.text = "Score: " + _score;
	}

	private IEnumerator RemoveGameMessage()
	{
		RemoveGameMessage_running = true;
		yield return new WaitForSeconds(1f);

		_processLabel.text = "";
		RemoveGameMessage_running = false;
	}

	private void UpdateRoundTime()
	{
		float newRoundTime = ROUND_TIME - _score * 0.5f;	//0.5f - just a random value

		_roundTime = newRoundTime >= MIN_ROUND_TIME ? newRoundTime : MIN_ROUND_TIME;
	}

	private IEnumerator CalculatingRoundTime()
	{
		while(_roundTime > 0f)	{

			float timeFrame = 0.1f;
			_roundTime -= timeFrame;
			_timeLabel.text = "Time: " + _roundTime.ToString("0.0");

			yield return new WaitForSeconds(timeFrame);
		}

		GameOver();
	}

	private void GameOver()
	{
		_gameOverCanvas.SetActive(true);
		_gameCanvas.SetActive(false);
		_mainMenuCanvas.SetActive(false);

		_totalScoreLabel.text = "Score: " + _score.ToString();
	}

}
