﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Figure : MonoBehaviour {

	public enum FigureType	{
		NONE = -1,
		Square,
		Triangle,
		Circle
	}

	private float drawing_accuracy = 1f;
	private Texture2D _template;
	private FigureType _figureType = FigureType.NONE;
	
	public Figure(FigureType ft, float acc)	{
		_figureType = ft;
		drawing_accuracy = acc;

		_template = Resources.Load("Templates/template_" + (int)ft) as Texture2D;
	}

	public void ShowFigure(Image outputImage)
	{
		Debug.Log("ShowFigure = " + _figureType);
		
		outputImage.sprite = Sprite.Create(_template, new Rect(0, 0, _template.width, _template.height), new Vector2(0.5f, 0.5f));
	}

	public bool IsFigureCapable(List<Vector3> drawedPoints)
	{
		bool figureClosed = Vector3.Distance(drawedPoints[0], drawedPoints[drawedPoints.Count - 1]) < 0.3f;	//geometric figure should be closed
		if(!figureClosed)	{
			Debug.Log("Figure wasn't closed");
			return false;
		}

		float bottomHeight = drawedPoints[0].y;
		float topHeight = drawedPoints[0].y;
		float leftWidth = drawedPoints[0].x;
		float rightWidth = drawedPoints[0].x;
		
		for(int i = 1; i < drawedPoints.Count; i++)	{
			
			//finding the bottom
			if(drawedPoints[i].y < bottomHeight)	{	bottomHeight = drawedPoints[i].y;	}
			//finding the top
			if(drawedPoints[i].y > topHeight)	{	topHeight = drawedPoints[i].y;	}
			//finding left side
			if(drawedPoints[i].x < leftWidth)	{	leftWidth = drawedPoints[i].x;	}
			//finding right side
			if(drawedPoints[i].x > rightWidth)	{	rightWidth = drawedPoints[i].x;	}
		}
		
		float fullWidthFigure = rightWidth - leftWidth;
		float fullHeightFigure = topHeight - bottomHeight;

		float percentScale = fullWidthFigure >= fullHeightFigure ? fullWidthFigure/_template.width : fullHeightFigure/_template.height;
		List<Vector2> transformedPoints = new List<Vector2>();
		
		for(int i = 0; i < drawedPoints.Count; i++)	{
			
			Vector2 newP = new Vector2((int)((drawedPoints[i].x - leftWidth)/percentScale), (int)((drawedPoints[i].y - bottomHeight)/percentScale));
			transformedPoints.Add(newP);
		}
		
		int correctPointsCount = 0;
		//after points were transformed to the resolution of the template, we can check if drawed figure was capable enough
		for(int i = 0; i < transformedPoints.Count; i++)
		{
			if(_template.GetPixel((int)transformedPoints[i].x, (int)transformedPoints[i].y).a > 0f)
			{
				correctPointsCount++;
			}
		}
		
		Debug.Log("CorrectPointsCount = " + correctPointsCount + " transformedPoints = " + transformedPoints.Count);
		
		if(transformedPoints.Count * drawing_accuracy <= correctPointsCount)
		{
			Debug.Log("It's right figure!");
			return true;
		}
		else {
			Debug.Log("It's wrong figure!");
			return false;
		}
		
	}
}